//
//  AppDelegate.m
//  iOSApp
//
//  Created by Jurayev Nodir on 12/18/15.
//  Copyright © 2015 Jurayev Nodir. All rights reserved.
//

#import "AppDelegate.h"
#import "HomeViewController.h"
#import "CustomNavigationViewController.h"
#import "CoreDataManager.h"
#import "CustomNavigationViewController.h"
#import "MenuViewController.h"
#import "LoginViewController.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import "LTHPasscodeViewController.h"
#import <LocalAuthentication/LAContext.h>

@interface AppDelegate ()<LTHPasscodeViewControllerDelegate>{
    UIImageView *_blurView;
}

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    _blurView = [[UIImageView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    [self.window makeKeyAndVisible];
    [self defaultMethod];
    BOOL auth = YES;
    if (auth) {
        [self initPassCodeView];
        if ([LTHPasscodeViewController doesPasscodeExist] &&
            [LTHPasscodeViewController didPasscodeTimerEnd]) {
            [[LTHPasscodeViewController sharedUser] showLockScreenWithAnimation:YES
                                                                     withLogout:NO
                                                                 andLogoutTitle:nil];
        }
        [self homeController];
//        [self showLockViewForEnablingPasscode];
        
    } else{
        [self loginController];
    }
    [SocketManager instance];
     
    
    UILocalNotification *notification = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    if (notification) {
        NSDictionary *userInfo = (NSDictionary *)notification;
        NSInteger cid = [userInfo[@"cid"] integerValue];
        if (cid>0) {
            [self performSelector:@selector(receivedPush:) withObject:userInfo afterDelay:1];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"push" object:@(cid)];
        }
        
    }
    
    NSString *token = [[SettingsManager instance] objForKey:kDeviceToken];
    if (token) {
        NSLog(@"%@",token);
    }
    

    return YES;
}

-(void)receivedPush:(NSDictionary*)userInfo{
    NSInteger cid = [userInfo[@"cid"] integerValue];
    if (cid>0) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"push" object:@(cid)];
    }
}
-(UIStoryboard*)mainStoryboard{
    if (_mainStoryBoard) {
        return _mainStoryBoard;
    }
    NSBundle *bundle = [NSBundle mainBundle];
    NSString *storyboardName = @"Main";
    _mainStoryBoard = [UIStoryboard storyboardWithName:storyboardName bundle:bundle];
    return _mainStoryBoard;
}

-(void)loginController{
    LoginViewController *controller = [[self mainStoryboard] instantiateViewControllerWithIdentifier:@"LoginViewController"];
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:controller];
    navController.navigationBar.translucent = YES;
    self.window.rootViewController = navController;
}

-(void)homeController{

    HomeViewController *controller = [[self mainStoryboard] instantiateViewControllerWithIdentifier:@"HomeViewController"];
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:controller];
    navController.navigationBar.translucent = YES;
    self.window.rootViewController = navController;
}

-(void)defaultMethod {
    [CoreDataManager sharedManager].modelName = @"DataBase";
    [CoreDataManager sharedManager].databaseName = @"DataBase";
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
    [Fabric with:@[[CrashlyticsKit class]]];
    
    
    [[UINavigationBar appearance] setBarTintColor:[UIColor whiteColor]];
    [[UINavigationBar appearance] setTitleTextAttributes:@{NSFontAttributeName:[UIFont helveticeFontSize:20 bold:YES],NSForegroundColorAttributeName:[UIColor whiteColor]}];
    [[UIBarButtonItem appearance] setTintColor:[UIColor whiteColor]];
}


-(void)splashController{
//    SplashScreenViewController *controller = [[SplashScreenViewController alloc] init];
//    CustomNavigationViewController *navController = [[CustomNavigationViewController alloc] initWithRootViewController:controller];
//    self.window.rootViewController = navController;
    
}

#pragma mark - PassCode  methods
-(void)initPassCodeView{
    [LTHPasscodeViewController sharedUser].delegate = self;
    [LTHPasscodeViewController sharedUser].maxNumberOfAllowedFailedAttempts = 3;

}
- (void)_turnOffPasscode {
    [self showLockViewForTurningPasscodeOff];
}


- (void)_changePasscode {
    [self showLockViewForChangingPasscode];
}


- (void)_enablePasscode {
    [self showLockViewForEnablingPasscode];
}


- (void)_testPasscode {
    [self showLockViewForTestingPasscode];
}

- (void)_switchPasscodeType:(UISwitch *)sender {
//    [[LTHPasscodeViewController sharedUser] setIsSimple:sender.isOn
//                                       inViewController:self
//                                                asModal:YES];
}

- (void)_touchIDPasscodeType:(UISwitch *)sender {
    [[LTHPasscodeViewController sharedUser] setAllowUnlockWithTouchID:sender.isOn];
}

- (void)showLockViewForEnablingPasscode {
    UINavigationController *navController = (UINavigationController*)self.window.rootViewController;
    [[LTHPasscodeViewController sharedUser] showForEnablingPasscodeInViewController:navController.visibleViewController
                                                                            asModal:NO];
}


- (void)showLockViewForTestingPasscode {
    [[LTHPasscodeViewController sharedUser] showLockScreenWithAnimation:YES
                                                             withLogout:NO
                                                         andLogoutTitle:nil];
}


- (void)showLockViewForChangingPasscode {
    [[LTHPasscodeViewController sharedUser] showForChangingPasscodeInViewController:self.window.rootViewController asModal:YES];
}


- (void)showLockViewForTurningPasscodeOff {
    [[LTHPasscodeViewController sharedUser] showForDisablingPasscodeInViewController:self.window.rootViewController
                                                                             asModal:NO];
}
- (UIImage *) imageWithView:(UIView *)view
{
    UIGraphicsBeginImageContextWithOptions(view.bounds.size, view.opaque, 0.0);
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage * img = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return img;
}

- (UIImage*) blur:(UIImage*)theImage
{
    
    // ***********If you need re-orienting (e.g. trying to blur a photo taken from the device camera front facing camera in portrait mode)
    // theImage = [self reOrientIfNeeded:theImage];
    
    // create our blurred image
    CIContext *context = [CIContext contextWithOptions:nil];
    CIImage *inputImage = [CIImage imageWithCGImage:theImage.CGImage];
    
    // setting up Gaussian Blur (we could use one of many filters offered by Core Image)
    CIFilter *filter = [CIFilter filterWithName:@"CIGaussianBlur"];
    [filter setValue:inputImage forKey:kCIInputImageKey];
    [filter setValue:[NSNumber numberWithFloat:15.0f] forKey:@"inputRadius"];
    CIImage *result = [filter valueForKey:kCIOutputImageKey];
    
    // CIGaussianBlur has a tendency to shrink the image a little,
    // this ensures it matches up exactly to the bounds of our original image
    CGImageRef cgImage = [context createCGImage:result fromRect:[inputImage extent]];
    
    UIImage *returnImage = [UIImage imageWithCGImage:cgImage];//create a UIImage for this function to "return" so that ARC can manage the memory of the blur... ARC can't manage CGImageRefs so we need to release it before this function "returns" and ends.
    CGImageRelease(cgImage);//release CGImageRef because ARC doesn't manage this on its own.
    
    return returnImage;
    
    // *************** if you need scaling
    // return [[self class] scaleIfNeeded:cgImage];
}

- (BOOL)isTouchIDAvailable {
    if ([[[UIDevice currentDevice] systemVersion] compare:@"8.0" options:NSNumericSearch] != NSOrderedAscending) {
        return [[[LAContext alloc] init] canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:nil];
    }
    return NO;
}

# pragma mark - LTHPasscodeViewController Delegates -

- (void)passcodeViewControllerWillClose {
    NSLog(@"Passcode View Controller Will Be Closed");
//    [self _refreshUI];
}

- (void)maxNumberOfFailedAttemptsReached {
    [LTHPasscodeViewController deletePasscodeAndClose];
    NSLog(@"Max Number of Failed Attemps Reached");
}

- (void)passcodeWasEnteredSuccessfully {
    NSLog(@"Passcode Was Entered Successfully");
}

- (void)logoutButtonWasPressed {
    NSLog(@"Logout Button Was Pressed");
}


- (void)applicationWillResignActive:(UIApplication *)application {
    _blurView.image = [self blur:[self imageWithView:self.window.rootViewController.view]];
    [self.window addSubview:_blurView];
    NSLog(@"ResignActive");
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    NSLog(@"Enter Background");
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    NSLog(@"Enter foreground");
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    [self showLockViewForEnablingPasscode];
    [_blurView removeFromSuperview];
    NSLog(@"Did becomeActive");
}

- (void)applicationWillTerminate:(UIApplication *)application {
    NSLog(@"Terminate");
}

@end
