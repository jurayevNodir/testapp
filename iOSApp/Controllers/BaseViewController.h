//
//  BaseViewController.h
//  Abeeh
//
//  Created by Admin on 19/01/15.
//  Copyright (c) 2015 ABEEH TEAM. All rights reserved.


#import <UIKit/UIKit.h>
#import "TPKeyboardAvoidingTableView.h"

@interface BaseViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>{
    TPKeyboardAvoidingTableView *_tableView;
}

@property (nonatomic, assign) NSInteger type;

- (void) reloadTableView;
- (void) addTableView;
- (UIView *)bgView;
- (void) setupNavigationbarItems;
- (void) back;
- (UIView*) findFirstResponderBeneathView:(UIView*)view;
- (void) changeLanguage:(id)notification;
- (void)changeNavBar:(UIColor *)color;
- (void)textFieldSettings:(UITextField *)field;

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu;

- (BOOL)slideNavigationControllerShouldDisplayRightMenu;


- (void)showLoaderText:(NSString *)text  title:(NSString*)title;
- (void)hideHUDTitle:(NSString*)title message:(NSString*)message;
- (void)shareText:(NSString *)text andImage:(UIImage *)image andUrl:(NSURL *)url;
@end
