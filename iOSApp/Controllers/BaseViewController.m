//
//  BaseViewController.m
//  Abeeh
//
//  Created by Admin on 19/01/15.
//  Copyright (c) 2015 ABEEH TEAM. All rights reserved.
//

#import "BaseViewController.h"
#import "SlideNavigationController.h"

@interface BaseViewController (){
 
}

@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [self.navigationController.navigationItem setHidesBackButton:YES animated:YES];
    [self.navigationItem setHidesBackButton:YES animated:YES];
    [self setNeedsStatusBarAppearanceUpdate];
    [self setupNavigationbarItems];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadTableView) name:@"sync" object:nil];
    self.view.backgroundColor = COLOR(239, 239, 239);
    [self reloadTableView];
}

-(UIView *)bgView{
    return [self.view viewWithTag:88];
}

-(void)reloadTableView{
    [_tableView reloadData];
}

-(void)changeNavBar:(UIColor *)color{

}
- (void)shareText:(NSString *)text andImage:(UIImage *)image andUrl:(NSURL *)url {
    NSMutableArray *sharingItems = [NSMutableArray new];
    
    if (text) {
        [sharingItems addObject:text];
    }
    if (image) {
        [sharingItems addObject:image];
    }
    if (url) {
        [sharingItems addObject:url];
    }
    
    UIActivityViewController *activityController = [[UIActivityViewController alloc] initWithActivityItems:sharingItems applicationActivities:nil];
    [[SlideNavigationController sharedInstance] presentViewController:activityController animated:YES completion:nil];
}


-(void)addTableView{
    _tableView = [[TPKeyboardAvoidingTableView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-64)];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.backgroundColor = [UIColor clearColor];
    _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    _tableView.separatorInset = UIEdgeInsetsZero;
    _tableView.separatorColor = COLOR(68, 70, 92);
    [self.view addSubview:_tableView];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    return cell;
}

-(void)textFieldSettings:(UITextField *)field{
    field.backgroundColor = [UIColor whiteColor];
    field.layer.cornerRadius = 5;
    UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 5)];
    field.leftView = leftView;
    field.leftViewMode = UITextFieldViewModeAlways;
    field.font = [UIFont helveticeFontSize:14 bold:NO];
    field.textColor = [[UIColor grayColor] colorWithAlphaComponent:0.8];
}

-(void)changeLanguage:(id)notification{
    
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationItem setHidesBackButton:YES animated:YES];
    if (self.type == 1) {
        self.navigationItem.leftBarButtonItems = [StyleManager styleBackItems:self selector:@selector(back)];
    }
    [self reloadTableView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIView*)findFirstResponderBeneathView:(UIView*)view {
    for ( UIView *childView in view.subviews ) {
        if ( [childView respondsToSelector:@selector(isFirstResponder)] && [childView isFirstResponder] ) return childView;
        UIView *result = [self findFirstResponderBeneathView:childView];
        if ( result ) return result;
    }
    return nil;
}

- (void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    [[self findFirstResponderBeneathView:self.view] resignFirstResponder];
    [super touchesEnded:touches withEvent:event];
}



-(void)setupNavigationbarItems{
    
}

#pragma mark - SlideNavigationController Methods -

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu {
    return  self.type != 1;
}

- (BOOL)slideNavigationControllerShouldDisplayRightMenu {
    return NO;
}

-(void)dealloc{
    NSLog(@"dealloc:%@",self.class);

}

-(void)showLoaderText:(NSString *)text  title:(NSString*)title{
    [self showHUDWithTitle:title
                   message:text];
}

-(void)hideHUDTitle:(NSString*)title message:(NSString*)message{
    [self hideHUDWithCompletionTitle:title
                             message:message
                     finishedHandler:NULL];
    
}



#pragma action methods
-(void)back{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
