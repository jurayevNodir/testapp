//
//  HomeViewController.m
//  iOSApp
//
//  Created by Jurayev Nodir on 12/18/15.
//  Copyright © 2015 Jurayev Nodir. All rights reserved.
//

#import "HomeViewController.h"
#import "TPKeyboardAvoidingTableView.h"

@interface HomeViewController (){
    
}

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    UILabel *label  = [[UILabel alloc] initWithFrame:CGRectMake(20, 100, self.view.width - 40, 35)];
    label.font = [UIFont helveticeFontSize:14 bold:NO];
    label.textAlignment = NSTextAlignmentCenter;
    label.text = @"This is Home Screen";
    [self.view addSubview:label];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
