//
//  LoginViewController.m
//  TestApp
//
//  Created by Jurayev Nodir on 1/22/16.
//  Copyright (c) 2016 Jurayev Nodir. All rights reserved.
//

#import "LoginViewController.h"
#import "TPKeyboardAvoidingScrollView.h"
@interface LoginViewController (){
    UITextField *_emailField;
    UITextField *_passwordField;
    TPKeyboardAvoidingScrollView *_scrollView;
}

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Вход";
    [self initSubViews];
}

-(void)initSubViews{
    _scrollView = [[TPKeyboardAvoidingScrollView alloc] initWithFrame:self.view.bounds];
    [self.view addSubview:_scrollView];
   
    UILabel *descLabel = [[UILabel alloc] initWithFrame:CGRectMake(30, 100, _scrollView.width - 60, 40)];
    descLabel.font = [UIFont helveticeFontSize:14 bold:NO];
    descLabel.text = @"Введите логин и пароль";
    descLabel.textColor = [UIColor whiteColor];
    descLabel.numberOfLines = 2;
    descLabel.textAlignment = NSTextAlignmentCenter;
    [_scrollView addSubview:descLabel];
    NSInteger top = 140;
    NSInteger padding= 45;
    _emailField = [[UITextField alloc] initWithFrame:CGRectMake(30, top, _scrollView.width - 60, 35)];
    _emailField.placeholder = @"Логин";
    _emailField.keyboardType = UIKeyboardTypeEmailAddress;
    [self textFieldSettings:_emailField];
    [_scrollView addSubview:_emailField];
    
    _passwordField = [[UITextField alloc] initWithFrame:CGRectMake(30, top +padding, _scrollView.width - 60, 35)];
    _passwordField.secureTextEntry = YES;
    _passwordField.placeholder = @"Пароль";
    [self textFieldSettings:_passwordField];
    [_scrollView addSubview:_passwordField];

    
    UIButton *loginBtn = [StyleManager defaultBtn:CGRectMake(0, top +2*padding+20, 120, 40) title:@"Войти"];
    [_scrollView addSubview:loginBtn];
    loginBtn.centerX = _scrollView.width/2;
    loginBtn.backgroundColor = [UIColor clearColor];
    [loginBtn addTarget:self action:@selector(login) forControlEvents:UIControlEventTouchUpInside];
    
    UITapGestureRecognizer *reg = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showForgot)];
    
    UIButton *registerBtn = [StyleManager defaultBtn:CGRectMake(0, top +3*padding+40, 120, 40) title:@"Регистрация"];
    [_scrollView addSubview:registerBtn];
    registerBtn.centerX = _scrollView.width/2;
    registerBtn.backgroundColor = [UIColor clearColor];
    [registerBtn addTarget:self action:@selector(showRegister) forControlEvents:UIControlEventTouchUpInside];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(40, registerBtn.top +40 + 2*padding, self.view.width -80, 20)];
    label.text = @"Забыли пароль";
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor whiteColor];
    label.font = [UIFont helveticeFontSize:14 bold:YES];
    label.adjustsFontSizeToFitWidth = YES;
    label.userInteractionEnabled = YES;
    [label addGestureRecognizer:reg];
    [_scrollView addSubview:label];
}


-(void)login{
    NSString *email = _emailField.text;
    if (![email isEmailValid]) {
        showError(@"Проверьте правильность своего email адреса");
        return;
    }
    NSString *password = _passwordField.text;
    if (password.length==0) {
         showError(@"Проверьте правильность паролей.");
        return;
    }
    
//    [self showHUD];
//    __weak typeof(self) weakSelf = self;
    NSString *uniqId = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    NSDictionary *data = @{@"email":@"232",@"password":password};
    NSDictionary *params = @{@"type":@"LOGIN_CUSTOMER", @"sequence_id":uniqId, @"data":data};
    [[SocketManager instance] login:params callback:^(id response, ErrorObj *error) {
//        [weakSelf hideHUD];
    }];
}

-(void)showCabinet{

}

-(void)showForgot{

}

-(void)showRegister{
 
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
     [self.navigationController.navigationBar setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault];
}
@end
