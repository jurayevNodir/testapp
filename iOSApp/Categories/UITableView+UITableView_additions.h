//
//  UITableView+UITableView_additions.h
//  Planet
//
//  Created by Admin on 14/10/14.
//  Copyright (c) 2014 Planet. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITableView (UITableView_additions)

-(void)reloadOnMainThread;

@end
