//
//  UITextView+UITextView_Aligment.h
//  TaxiApp
//
//  Created by Admin on 12/06/15.
//  Copyright (c) 2015 Jurayev Nodir. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextView (UITextView_Aligment)
- (void)alignToTop;
- (void)disableAlginment;
@end
