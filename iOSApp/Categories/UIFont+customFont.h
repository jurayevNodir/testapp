//
//  UIFont+customFont.h
//  SmartHome-iphone
//
//  Created by Jurayev on 8/16/13.
//  Copyright (c) 2013 DS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIFont (customFont){
    
}

+ (UIFont*)customFontSize:(float)size;
+ (UIFont *)leftFontSize:(float)size;
+ (UIFont*)fontBreeSerifRegularWithSize:(float)size;
+ (UIFont*)defaultFontWithSize:(float)size;
+(UIFont*)helveticeFontSize:(float)size bold:(BOOL)bold;
@end
