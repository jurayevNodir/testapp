//
//  UITableView+UITableView_additions.m
//  Planet
//
//  Created by Admin on 14/10/14.
//  Copyright (c) 2014 Planet. All rights reserved.
//

#import "UITableView+UITableView_additions.h"

@implementation UITableView (UITableView_additions)

-(void)reloadOnMainThread{
    [self performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:YES];
}

-(UIEdgeInsets)separatorInset{
    return  UIEdgeInsetsZero;
}

@end
