//
//  UIImage+UIImageHelper.h
//  imusthaveit
//
//  Created by Администратор on 7/29/14.
//  Copyright (c) 2014 Andrii Tishchenko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (UIImageHelper)

+ (UIImage *)GMSMarkerImage:(UIImage *)image;
+ (UIImage *)GMSMarkerImageWithString:(NSString *)string color:(UIColor *)color;
@end
