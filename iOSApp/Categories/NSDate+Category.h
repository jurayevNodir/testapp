//
//  NSDate+Category.h
//  TaxiApp
//
//  Created by Admin on 29/06/15.
//  Copyright (c) 2015 Jurayev Nodir. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (Category)

+ (BOOL)date:(NSDate *)date1 isSameDayAsDate:(NSDate *)date2;

@end
