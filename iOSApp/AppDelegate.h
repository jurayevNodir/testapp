//
//  AppDelegate.h
//  iOSApp
//
//  Created by Jurayev Nodir on 12/18/15.
//  Copyright © 2015 Jurayev Nodir. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>{
    UIStoryboard *_mainStoryBoard;
}

@property (strong, nonatomic) UIWindow *window;


@end

