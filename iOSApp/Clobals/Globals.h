//
//  Globals2.h
//  TaxiApp
//
//  Created by Admin on 09/06/15.
//  Copyright (c) 2015 Jurayev Nodir. All rights reserved.
//

#ifndef TaxiApp_Globals2_h
#define TaxiApp_Globals2_h

#define COLOR(r,g,b)     [UIColor colorWithRed:r/255.f green:g/255.f blue:b/255.f alpha:1.f]

#define appColor COLOR(61,81,165)
#define appBgColor COLOR(240,240,240)

#define translation(str)   [[SettingsManager instance] translate:str]
#define isLogin()          [[SettingsManager instance] userLogin]

#define trim(string) [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]

#define showError(string) UIAlertView *alert = [[UIAlertView alloc] initWithTitle:translation(@"stanError") message:string delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];[alert show];

#define showSuccess(string) UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Success" message:string delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];[alert show];

#define NULL_TO_NIL(obj)({ __typeof__ (obj) __obj = (obj); __obj == [NSNull null] ? nil : obj;})
#define NIL_TO_STRING(str) str == nil ? @"" : str;

#define IS_WIDESCREEN   (fabs((double)[[UIScreen mainScreen] bounds].size.height - (double)568) < DBL_EPSILON )

#define IS_IPHONE       ([[[UIDevice currentDevice] model] hasPrefix: @"iPhone"])
#define IS_IPOD         ([[[UIDevice currentDevice] model] isEqualToString: @"iPod touch"])

#define IS_IPHONE_5     (IS_IPHONE && IS_WIDESCREEN)


#define IPAD     (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) ? YES : NO
#define kPortraitWidth          [[UIScreen mainScreen] bounds].size.width



#define kk          (IS_IPHONE_5  ? 548.f : 460.f)

#define kPortraitHeight          (IPAD ?  768 : kk)

#define kPortraitNavbarHeight    44.f

#define kLandscapeWidth          (IS_IPHONE_5 ? 568.f : 480.f)
#define kLandscapeHeight         320.f
#define kLandscpaeNavbarHeight   32.f


#define iOS7             ([[[UIDevice currentDevice] systemVersion] floatValue] == 7) ? YES:NO
#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

#define PRELOADER_TAG       43211213

#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

#define DATE_FORMAT_HOURS @"HH:mm:ss"
#define DATE_FORMAT_STANDART @"dd.MM.yyyy"
#define DATE_FORMAT_MONTH_SHORT @"dd/MM/yyyy"
#define DATE_FORMAT_MONTH_SHORT_2 @"yyyy/MMM/dd"
#define DATE_FORMAT_MONTH_WITH_TIME @"yyyy/MMM/dd HH:mm"
#define DATE_FORMAT_MONTH_WITH_FULL_TIME @"yyyy-MMM-dd HH:mm:ss"
#define DATE_FORMAT_SHORT_MONTH_WITH_FULL_TIME @"yyyy/MM/dd HH:mm:ss"
#define DATE_FORMAT_SHORT_MONTH_WITH_TIME @"yyyy/MMM/dd HH:mm"
#define DATE_FORMAT_SHORT_WEEK_WITH_MONTH_WITH_TIME @"EEE, MMM-dd-yyyy, hh:mm"
#define DATE_FORMAT_SHORT_WEEK_WITH_MONTH_WITH_TIME_AM @"EEE, MMM-dd-yyyy, hh:mm a"
#define DATE_FORMAT_SHORT_WEEK_WITH_MONTH @"EEE, MMM-dd-yyyy"
#define DATE_FORMAT_SHORT_WEEK @"EEE dd MMM"
#define DATE_FORMAT_FULL_WEEK_MONTH @"EEEE, d MMMM"
#define DATE_FORMAT_ONLY_MONTH @"MMMM"
#define DATE_FORMAT_SERVERFORMAT @"MMM-dd-yyyy HH:mm:ss a"

#endif
