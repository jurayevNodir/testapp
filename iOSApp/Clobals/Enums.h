//
//  Enums.h
//  iOSApp
//
//  Created by Admin on 19/01/15.
//

#import <Foundation/Foundation.h>
#import "ActionButton.h"
#import "ImageViewBtn.h"
#import "ErrorObj.h"

typedef void (^Callback)(id response,ErrorObj *error);

typedef enum {
    RequestTypePOST,
    RequestTypeGET,
    RequestTypePUT,
    RequestTypeDELETE,
}RequestType;


typedef long long int ID;

typedef enum {
    navigationBarStyleWithbasket=0,
    navigationBarStyleNone,
    navigationBarStyleSearch,
    navigationBarStyleBackWithSearch,
    navigationBarStyleBack,
} NavigationBarStyle;




@interface Enums : NSObject{
    
}

@end
