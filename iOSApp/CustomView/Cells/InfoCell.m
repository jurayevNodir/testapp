//
//  InfoCell.m
//  iOSApp
//
//  Created by Admin on 04/03/15.
//  Copyright (c) 2015 Jurayev Nodir. All rights reserved.
//

#import "InfoCell.h"

@implementation InfoCell
-(id)initWithReuseIdentifier:(NSString *)reuseIdentifier info:(id)info{

    self = [super initWithReuseIdentifier:reuseIdentifier info:info];
    if (self) {
        self.contentView.backgroundColor = [UIColor whiteColor];
        self.nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 5, 230, 20)];
        self.nameLabel.font = [UIFont helveticeFontSize:14 bold:NO];
        self.nameLabel.backgroundColor = [UIColor clearColor];
        self.nameLabel.textColor = COLOR(79, 58, 99);
        [self.contentView addSubview:self.nameLabel];
        
        self.iconView = [[UIImageView alloc] initWithFrame:CGRectMake(290, 5, 20, 20)];
        self.iconView.image = [UIImage imageNamed:@"closeArrow.png"];
        [self.contentView addSubview:self.iconView];
        
        self.lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 31, 500, 0.5)];
        self.lineView.backgroundColor = [UIColor lightGrayColor];
        [self.contentView addSubview:self.lineView];
        
        self.descLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 25, 310, 30)];
        self.descLabel.font = [UIFont helveticeFontSize:14 bold:NO];
        self.descLabel.backgroundColor = [UIColor clearColor];
        self.descLabel.textColor = COLOR(18, 16, 19);
        self.descLabel.hidden = YES;
        self.descLabel.text = @"sf knr weiu riweut wiernut iwet wietweitnw eitnweitn weirutn weiutwer it";
        self.descLabel.numberOfLines = 0;
//        self.descLabel.lineBreakMode = NSLineBreakByWordWrapping;
        [self.contentView addSubview:self.descLabel];


    }
    
    return self;
}

@end
