//
//  ItemListCell.h
//  TaxiApp
//
//  Created by Admin on 19/06/15.
//  Copyright (c) 2015 Jurayev Nodir. All rights reserved.
//

#import "BaseCell.h"

@interface ItemListCell : BaseCell

@property(nonatomic, strong) UILabel *descLabel;
@property (nonatomic, strong) ImageViewBtn *deleteIcon;

@end
