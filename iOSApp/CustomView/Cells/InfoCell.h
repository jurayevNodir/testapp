//
//  InfoCell.h
//  iOSApp
//
//  Created by Admin on 04/03/15.
//  Copyright (c) 2015 Jurayev Nodir. All rights reserved.
//

#import "BaseCell.h"

@interface InfoCell : BaseCell
@property (nonatomic,strong) UILabel        *nameLabel;
@property (nonatomic,strong) UILabel        *descLabel;
@property (nonatomic,strong) UIView         *lineView;
@property (nonatomic,strong) UIImageView    *iconView;
@end
