//
//  MenuCell.h
//  iOSApp
//
//  Created by Admin on 19/01/15.
//  Copyright (c) 2015 Jurayev Nodir. All rights reserved.
//

#import "BaseCell.h"



@interface MenuCell : BaseCell{
    UIImageView *_arrowView;
}
+(float)cellHeight;

@property(nonatomic, strong) UILabel      *categoryLabel;
@property(nonatomic, strong) UIImageView  *iconView;
@property(nonatomic, assign) BOOL isSelected;

@end
