//
//  MenuCell.m
//  iOSApp
//
//  Created by Admin on 19/01/15.
//  Copyright (c) 2015 Jurayev Nodir. All rights reserved.
//

#import "MenuCell.h"


@implementation MenuCell


+(float)cellHeight{
    return 50;
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.iconView = [[UIImageView alloc] initWithFrame:CGRectMake(7.5, 7.5, 25, 25)];
        self.iconView.contentMode = UIViewContentModeScaleAspectFit;
        self.iconView.image = [UIImage imageNamed:@"ic_money_bag.png"];
        [self.contentView addSubview:self.iconView];
        
        
        self.categoryLabel = [[UILabel alloc] initWithFrame:CGRectMake(40, 5, 200, 30)];
        self.categoryLabel.font = [UIFont helveticeFontSize:16 bold:NO];
        self.categoryLabel.backgroundColor = [UIColor clearColor];
        self.categoryLabel.textAlignment = NSTextAlignmentLeft;
        self.categoryLabel.adjustsFontSizeToFitWidth = YES;
        [self.contentView addSubview:self.categoryLabel];

        _arrowView = [[UIImageView alloc] initWithFrame:CGRectMake(240, 17.5, 7, 15)];
        _arrowView.image = [UIImage imageNamed:@"arrow.png"];
        [self.contentView addSubview:_arrowView];
        
    }
    return self;
}

-(void)selectedStyle{
//    self.categoryLabel.textColor = COLOR(172, 174, 235);
//    _arrowView.image = [UIImage imageNamed:@"arrow_selected.png"];
    
}

-(void)normalStyle{
//    self.categoryLabel.textColor = [UIColor whiteColor];
//    _arrowView.image = [UIImage imageNamed:@"arrow.png"];
}

-(void)setSelected:(BOOL)selected animated:(BOOL)animated{
    [super setSelected:selected animated:animated];
    
    if (selected) {
        [self selectedStyle];
    } else{
        [self normalStyle];
    }
}

-(void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated{
    [super setHighlighted:highlighted animated:animated];
    
    if (highlighted) {
//        [self selectedStyle];
    } else{
//        [self normalStyle];
    }
}
@end
