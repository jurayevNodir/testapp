//
//  ItemListCell.m
//  TaxiApp
//
//  Created by Admin on 19/06/15.
//  Copyright (c) 2015 Jurayev Nodir. All rights reserved.
//

#import "ItemListCell.h"

@implementation ItemListCell
-(id)initWithReuseIdentifier:(NSString *)reuseIdentifier info:(id)info{
    self = [super initWithReuseIdentifier:reuseIdentifier info:info];
    if (self) {
        self.descLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 5, _width-45, 20)];
        self.descLabel.font = [UIFont helveticeFontSize:14 bold:NO];
        self.descLabel.textColor = appColor;
        self.descLabel.numberOfLines = 4;
        [self.contentView addSubview:self.descLabel];
        self.deleteIcon = [[ImageViewBtn alloc] initWithFrame:CGRectMake(_width-35, 5, 30, 30)];
        self.deleteIcon.iconPadding = 5;
        self.deleteIcon.iconView.image = [UIImage imageNamed:@"ic_white_trash.png"];
        self.deleteIcon.backgroundColor = appColor;
        [self.contentView addSubview:self.deleteIcon];
    }
    return self;
}
-(void)layoutSubviews{
    [super layoutSubviews];
    self.deleteIcon.centerY = self.contentView.height/2;
    self.descLabel.centerY = self.contentView.height/2;
}

@end
