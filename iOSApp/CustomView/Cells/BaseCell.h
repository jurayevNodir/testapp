//
//  BaseCell.h
//  iOSApp
//
//  Created by Admin on 19/01/15.
//  Copyright (c) 2015 Jurayev Nodir. All rights reserved.
//

#import <UIKit/UIKit.h>
#define kCellWidth     @"width"
#define kCellHeight    @"height"

@interface BaseCell : UITableViewCell{
    float _width;
    float _height;
}

-(id)initWithReuseIdentifier:(NSString *)reuseIdentifier info:(id)info;

@end
