//
//  ActionButton.m
//  TaxiApp
//
//  Created by Admin on 17/06/15.
//  Copyright (c) 2015 Jurayev Nodir. All rights reserved.
//

#import "ActionButton.h"

@implementation ActionButton

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    
    if (self) {
        CGSize size = frame.size;
        
        self.label = [[UILabel alloc] initWithFrame:CGRectMake(size.width*0.25, 5, size.width*0.75, size.height-10)];
        self.label.font = [UIFont helveticeFontSize:14 bold:NO];
        self.label.numberOfLines = 2;
        self.label.textColor = [UIColor whiteColor];
        self.label.textAlignment = NSTextAlignmentCenter;
        [self addSubview:self.label];
        
        self.iconView = [[UIImageView alloc] initWithFrame:CGRectMake(5, 5, 30, 30)];
        self.iconView.contentMode = UIViewContentModeScaleAspectFit;
        self.iconView.centerY = size.height/2;
        [self addSubview:self.iconView];
        
        self.layer.cornerRadius = 8;
        self.layer.borderColor = homeValueColor.CGColor;
        self.layer.borderWidth = 2;
        self.clipsToBounds = YES;
        UIImage *image = [StyleManager styleImageWithColor:appColor size:frame.size];
        [StyleManager styleBtn:self bgImage:image];
    }
    return self;
}
-(void)setFrame:(CGRect)frame{
    [super setFrame:frame];
     CGSize size = frame.size;
    self.iconView.centerY = size.height/2;
    self.label.frame = CGRectMake(size.width*0.25, 5, size.width*0.75, size.height-10);
}
@end
