//
//  ActionButton.h
//  TaxiApp
//
//  Created by Admin on 17/06/15.
//  Copyright (c) 2015 Jurayev Nodir. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ActionButton : UIButton{

}

@property(nonatomic, strong)  UILabel *label;
@property(nonatomic, strong) UIImageView *iconView;

@end
