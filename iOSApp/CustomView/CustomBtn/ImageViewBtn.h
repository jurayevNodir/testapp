//
//  ImageViewBtn.h
//  TaxiApp
//
//  Created by Admin on 17/06/15.
//  Copyright (c) 2015 Jurayev Nodir. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageViewBtn : UIButton

@property(nonatomic, strong) UIImageView *iconView;
@property(nonatomic, assign) float iconPadding;
@end
