//
//  ImageViewBtn.m
//  TaxiApp
//
//  Created by Admin on 17/06/15.
//  Copyright (c) 2015 Jurayev Nodir. All rights reserved.
//

#import "ImageViewBtn.h"

@implementation ImageViewBtn
- (instancetype)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.iconView = [[UIImageView alloc] initWithFrame:CGRectMake(5, 5, 30, 30)];
        [self addSubview:self.iconView];
         self.iconView.center = CGPointMake(frame.size.width/2, frame.size.height/2);
        
        self.layer.cornerRadius = 8;
        self.clipsToBounds = YES;
        self.backgroundColor = appColor;
        self.iconPadding = 5;


    }
    return self;
}
-(void)setFrame:(CGRect)frame{
    [super setFrame:frame];
    self.iconView.center = CGPointMake(frame.size.width/2, frame.size.height/2);
}

-(void)setIconPadding:(float)iconPadding{
    _iconPadding = iconPadding;
    CGSize size = self.frame.size;
    self.iconView.frame = CGRectMake(_iconPadding, _iconPadding, size.width-2*iconPadding, size.height-2*iconPadding);
}
@end
