//
//  GTScrollNavigationBar.h
//  GTScrollNavigationBar
//
//  Created by Luu Gia Thuy on 21/12/13.
//  Copyright (c) 2013 Luu Gia Thuy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomBadge.h"

typedef enum {
    GTScrollNavigationBarNone,
    GTScrollNavigationBarScrollingDown,
    GTScrollNavigationBarScrollingUp
} GTScrollNavigationBarState;

@interface GTScrollNavigationBar : UINavigationBar{
    int _oldType;
}

@property (strong, nonatomic) UIScrollView *scrollView;
@property (assign, nonatomic) GTScrollNavigationBarState scrollState;
@property (assign, nonatomic) NavigationBarStyle style;

// custom Views
@property (strong, nonatomic) UIButton     *searchBtn;
@property (strong, nonatomic) UIButton     *cancelBtn;
@property (strong, nonatomic) UIImageView  *logoView;
@property (strong, nonatomic) UIImageView  *cartView;
@property (strong, nonatomic) UILabel      *badgeView;
@property (strong, nonatomic) UILabel      *priceLabel;
@property (strong, nonatomic) UISearchBar  *searchField;
@property (strong, nonatomic) UIButton     *backView;
@property (strong, nonatomic) UIView       *basketContainer;

@property (nonatomic,copy) void (^backBlock)(id info);

/**
 * @deprecated use resetToDefaultPositionWithAnimation: instead
 * @see resetToDefaultPositionWithAnimation:
 */
- (void)resetToDefaultPosition:(BOOL)animated __attribute__((deprecated));

- (void)resetToDefaultPositionWithAnimation:(BOOL)animated;

@end

@interface UINavigationController (GTScrollNavigationBarAdditions)

@property(strong, nonatomic, readonly) GTScrollNavigationBar *scrollNavigationBar;

@end
