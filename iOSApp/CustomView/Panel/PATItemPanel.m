//
//  PMItemPanelViewController.m
//  PMO
//
//  Created by Nodir Jurayev on 10/16/12.
//  Copyright (c) 2012 Datasite Technology. All rights reserved.
//

#import "PATItemPanel.h"

@interface PATItemPanel ()<UITextFieldDelegate>
- (CGFloat)tableViewWidth;
@end

@implementation PATItemPanel


//@synthesize selectedIndex = self.selectedIndex;
//@synthesize indexPath = self.indexPath;
//@synthesize isAlphabetic = self.isAlphabetic;
//@synthesize cellType = self.cellType;
//@synthesize isLandscapePanel = self.isLandscapePanel;
//@synthesize isDescriptionDetails = self.isDescriptionDetails;



- (id)init {
    self = [super init];
    if (self) {
        _indexArray = [[NSMutableArray alloc] initWithCapacity:0];
        _sectionsContainer = [[NSMutableDictionary alloc] initWithCapacity:0];
        self.isAlphabetic = NO;
 		self.cellType = [[NSString alloc] init];
        
    }
    return self;
}


- (CGFloat)tableViewWidth {
	CGFloat width = 0;
	if (self.isLandscapePanel)
		width = 320;
	else
		width = 260;
	
	return width;
}

#pragma mark -
#pragma mark View Handling

- (void)dismissPopupViewControllerWithanimationType:(MJPopupViewAnimation)animationType
{
    UIView *popupView = [_sourceView viewWithTag:kMJPopupViewTag];
    UIView *overlayView = [_sourceView viewWithTag:kMJOverlayViewTag];

    [self fadeViewOut:popupView sourceView:_sourceView overlayView:overlayView];
}

- (void)presentInController:(UIViewController *)ctrl animationType:(MJPopupViewAnimation)animationType
{
    // customize popupView
    _tableView = [[UITableView alloc] init];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.layer.borderColor = COLOR(203.f, 199.f, 182.f).CGColor;
    _tableView.layer.borderWidth = 1.f;
    _tableView.layer.cornerRadius = 5.f;
    _tableView.alpha = 0.0f;

    if ([_tableView respondsToSelector:@selector(separatorInset)]) {
        _tableView.separatorInset = UIEdgeInsetsZero;
    }
   	CGFloat width = [self tableViewWidth];
    NSInteger count = self.items.count;
    count = count > 8 ? 8 : count;
	CGFloat height = (self.isAlphabetic) ? 414.f : count * 45.f;
    
	if (self.isDescriptionDetails) {
		height = 0;
		for (NSString *item in self.items) {
          CGSize size = [StyleManager size:item with:width-20 fontSize:15];
//			CGSize textLblSize = [item sizeWithFont:[UIFont systemFontOfSize:15] constrainedToSize:CGSizeMake(width - 20, MAXFLOAT)];
			height += size.height + 28;
		}
		
	}
    
    if (self.isLandscapePanel && height > 225) {
        height = 225;
    }
    
    if (self.requeredSelect) {
        height = 90;
    }
    _tableView.frame = CGRectMake(0.f, 0.f, width, height);
//    _tableView.centerY = kPortraitHeight/2;

    UIView *shadowView = [[UIView alloc] init];
    shadowView.frame = _tableView.frame;
    shadowView.layer.cornerRadius = 5.f;
    shadowView.layer.shadowPath = [UIBezierPath bezierPathWithRect:CGRectOffset(shadowView.frame, 0.f, 7.f)].CGPath;
    shadowView.layer.shadowRadius = 5.f;
    shadowView.layer.shadowColor = [[UIColor blackColor] CGColor];
    shadowView.layer.shadowOpacity =  0.8f;
    shadowView.tag = kMJShadowViewTag;
    
    if (ctrl.parentViewController==nil) {
        _sourceView = ctrl.view;
    }else{
        _sourceView = ctrl.parentViewController.view;
    }
    
    _sourceView.tag = kMJSourceViewTag;
    _tableView.tag = kMJPopupViewTag;
    _sourceView.backgroundColor = [UIColor clearColor];

    // check if source view controller is not in destination
    //if ([sourceView.subviews containsObject:_tableView]) return;
    
    // Add semi overlay
    UIView *overlayView = [[UIView alloc] initWithFrame:_sourceView.bounds];
    overlayView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    overlayView.tag = kMJOverlayViewTag;
    overlayView.backgroundColor = [UIColor clearColor];
    overlayView.clipsToBounds = YES;
    
    _tableView.clipsToBounds= YES;
    
    // BackgroundView
    MJPopupBackgroundView *backgroundView = [[MJPopupBackgroundView alloc] initWithFrame:_sourceView.bounds];
    backgroundView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    backgroundView.tag = kMJBackgroundViewTag;
    backgroundView.backgroundColor = [UIColor clearColor];
    backgroundView.alpha = 0.0f;
    
    if (iOS7) {
        overlayView.height+=20;
        backgroundView.height+=20;
    }


    // Make the Background Clickable
    UIButton * dismissButton = [UIButton buttonWithType:UIButtonTypeCustom];
    dismissButton.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    dismissButton.backgroundColor = [UIColor clearColor];
    dismissButton.frame = _sourceView.bounds;

    UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:_sourceView.bounds];
    scrollView.backgroundColor = [UIColor clearColor];
    scrollView.userInteractionEnabled = YES;
    CGRect rect = scrollView.frame;
    rect.size.height += 1;
    scrollView.contentSize = rect.size;
    [scrollView addSubview:dismissButton];
    [scrollView addSubview:shadowView];
    [scrollView addSubview:_tableView];
    
 
    
    
    self.alpha = 0.f;
    self.frame = overlayView.frame;

    [overlayView addSubview:backgroundView];
    [overlayView addSubview:scrollView];
    [self addSubview:overlayView];
    [_sourceView addSubview:self];
    
    [dismissButton addTarget:self action:@selector(dismissPopupViewControllerWithanimation:) forControlEvents:UIControlEventTouchUpInside];

    dismissButton.tag = MJPopupViewAnimationFade;
    [self fadeViewIn:_tableView sourceView:_sourceView overlayView:overlayView];
}

- (void)dismissPopupViewControllerWithanimation:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(panelDismissed)])
        [self.delegate panelDismissed];

    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
}

- (void)setItems:(NSArray *)items {
    
    if (self.isAlphabetic) {
    } else {
        _items = items;
    }
}

#pragma mark --- Fade

- (void)fadeViewIn:(UIView*)popupView sourceView:(UIView*)sourceView1 overlayView:(UIView*)overlayView
{
    UIView *backgroundView = [overlayView viewWithTag:kMJBackgroundViewTag];
    // Generating Start and Stop Positions
    CGSize sourceSize = _sourceView.bounds.size;
    CGSize popupSize = popupView.bounds.size;
    CGRect popupEndRect = CGRectMake((sourceSize.width - popupSize.width) / 2,
                                     (sourceSize.height - popupSize.height) / 2,
                                     popupSize.width,
                                     popupSize.height);
    
    // Set starting properties
    popupView.frame = popupEndRect;
    popupView.alpha = 0.0f;
    
    UIView *shadowView = [overlayView viewWithTag:kMJShadowViewTag];
    shadowView.frame = popupEndRect;
    
    [UIView animateWithDuration:kPopupModalAnimationDuration animations:^{
        backgroundView.alpha = 0.7f;
        popupView.alpha = 1.0f;
        shadowView.alpha = 1.f;
        self.alpha = 1.f;
    } completion:^(BOOL finished) {
    }];
     
}

- (void)fadeViewOut:(UIView*)popupView sourceView:(UIView*)sourceView overlayView:(UIView*)overlayView
{
    UIView *shadowView = [overlayView viewWithTag:kMJShadowViewTag];
    UIView *backgroundView = [overlayView viewWithTag:kMJBackgroundViewTag];
    [UIView animateWithDuration:kPopupModalAnimationDuration animations:^{
        backgroundView.alpha = 0.0f;
        popupView.alpha = 0.0f;
        self.alpha = 0.0f;
        shadowView.alpha = 0.f;
    } completion:^(BOOL finished) {
        [popupView removeFromSuperview];
        [overlayView removeFromSuperview];
        [self removeFromSuperview];
    }];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    if (self.requeredSelect) {
        return 1;
    }
    return self.isAlphabetic ? [_indexArray count] : 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.requeredSelect) {
        return 1;
    }
    // Return the number of rows in the section.
    return self.isAlphabetic ? [[_sectionsContainer objectForKey:[_indexArray objectAtIndex:section]] count] : [self.items count];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (self.requeredSelect) {
        
    }
    
    if (self.isAlphabetic) {
        UIImage *bgImg = [[UIImage imageNamed:@"bg-section-pt.png"] stretchableImageWithLeftCapWidth:0.f topCapHeight:0.f];
        UIImageView *headerImageView = [[UIImageView alloc] initWithImage:bgImg];
        headerImageView.alpha = 0.8f;
        headerImageView.frame = CGRectMake(0.f, 0.f, tableView.frame.size.width, bgImg.size.height);
        headerImageView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        
        UILabel *dateLb = [[UILabel alloc] initWithFrame:CGRectMake(7.f, 4.f, tableView.frame.size.width, 16.f)];
        dateLb.textAlignment = NSTextAlignmentLeft;
        dateLb.textColor = [UIColor clearColor];
        dateLb.backgroundColor = [UIColor clearColor];
        dateLb.font = [UIFont customFontSize:16];
        dateLb.text = [_indexArray objectAtIndex:section];
        dateLb.shadowColor = [UIColor whiteColor];
        dateLb.shadowOffset = CGSizeMake(0.f, 1.f);
        [headerImageView addSubview:dateLb];
        
        return headerImageView;
    }
    
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (self.requeredSelect) {
        return 45;
    }
    return self.isAlphabetic ? 22.f : 0;
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
   
    return _indexArray;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

//    CustomCellBackgroundViewPosition position;
//    position = CustomCellBackgroundViewPositionMiddle;
    if (self.requeredSelect) {
        NSString *CellIdentifier = [NSString stringWithFormat:@"TextField"];
        
        BaseCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (!cell) {
            cell = [[BaseCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
            //cell.position = position;
        }
        UITextField *textField = [[UITextField alloc] initWithFrame:CGRectMake(5,3, 250, 35)];
        textField.delegate  = self;
        textField.placeholder = @"Password";
        textField.returnKeyType = UIReturnKeyDone;
        textField.secureTextEntry = YES;
        [cell.contentView addSubview:textField];
        return cell;
        
    }
    
    NSString *CellIdentifier = [NSString stringWithFormat:@"Cell"];
    
    BaseCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell) {
        cell = [[BaseCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.textLabel.numberOfLines = 2;
        //cell.position = position;
    }

    
    id obj = nil;
    if (self.isAlphabetic) {
        obj = [[_sectionsContainer objectForKey:[_indexArray objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
    } else {
        obj = [self.items objectAtIndex:indexPath.row];
    }
    cell.textLabel.text = obj;
    cell.textLabel.textColor = appColor;
      
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	CGFloat height = [StyleManager size:_items[indexPath.row] with:_tableView.width fontSize:14].height;
    if (height<45) {
        return 45;
    } else{
        return height + 5;
    }
    
//    if (self.isDescriptionDetails) {
//	 	NSString *item = [self.items objectAtIndex:indexPath.row];
//		CGSize textLblSize = [item sizeWithFont:[UIFont systemFontOfSize:15] constrainedToSize:CGSizeMake([self tableViewWidth] - 20, MAXFLOAT)];
//		height = textLblSize.height + 28;
//	}
//	
//    return height;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
      
    //id obj = nil;
    if (self.isAlphabetic) {
     //   obj = [[_sectionsContainer objectForKey:[_indexArray objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
    }
    
    if ([self.delegate respondsToSelector:@selector(panel:didSelectAtIndexPath: withObject:)]) {
        [self.delegate panel:self didSelectAtIndexPath:indexPath withObject:[NSNumber numberWithInteger:indexPath.row]];
    }
    
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
}

#pragma mark - UitextFieldDelegate
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    [UIView animateWithDuration:0.4 animations:^{
        _tableView.top -=50;
    }];

}
-(void)textFieldDidEndEditing:(UITextField *)textField{
    [UIView animateWithDuration:0.4 animations:^{
        _tableView.top += 50;
    }];
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    if (textField.text.length>0) {
        [self.delegate panel:self didSelectAtIndexPath:nil withObject:textField.text];
//        [self.delegate panelDismissed];
        [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
    }

    return YES;
}

-(void)dealloc{
    NSLog(@"Dealloc:%@",self.class);
}

@end

















