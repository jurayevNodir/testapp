//
//  PMItemPanelViewController.h
//  PMO
//
//  Created by Nodir Jurayev on 10/18/12.
//  Copyright (c) 2012 Datasite Technology. All rights reserved.
//


#import "MJPopupBackgroundView.h"
#import "BaseCell.h"

#define kPopupModalAnimationDuration 0.35
#define kMJSourceViewTag 23941
#define kMJPopupViewTag 23942
#define kMJBackgroundViewTag 23943
#define kMJOverlayViewTag 23945
#define kMJShadowViewTag 23946

@protocol PATItemPanelDelegate;
#import "UIViewController+MJPopupViewController.h"

@interface PATItemPanel : UIView <UITableViewDelegate, UITableViewDataSource> {
//    PATPanelType             _type;
    UITableView             *_tableView;
    UIView                  *_sourceView;
    NSMutableArray          *_indexArray;
    NSMutableDictionary     *_sectionsContainer;
}

@property (nonatomic, weak) id<PATItemPanelDelegate> delegate;
@property (nonatomic, assign) int selectedIndex;
@property (nonatomic, strong) NSIndexPath *indexPath;
@property (nonatomic, strong) NSArray *items;
@property (nonatomic, assign) BOOL isAlphabetic;
@property (nonatomic, strong) NSString *cellType;
@property (nonatomic, assign) BOOL isLandscapePanel;
@property (nonatomic, assign) BOOL isDescriptionDetails;
@property (nonatomic, assign) BOOL requeredSelect;

- (void)presentInController:(UIViewController *)ctrl animationType:(MJPopupViewAnimation)animationType;

@end


@protocol PATItemPanelDelegate <NSObject>
@optional
- (void)panel:(PATItemPanel *)panel didSelectAtIndexPath:(NSIndexPath *)indexPath withObject:(id)obj;
- (void)panelDismissed;
@end
