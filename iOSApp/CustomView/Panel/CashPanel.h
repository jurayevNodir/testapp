//
//  CashPanel.h
//  TaxiApp
//
//  Created by Jurayev Nodir on 8/5/15.
//  Copyright (c) 2015 Jurayev Nodir. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PATItemPanel.h"

@interface CashPanel : PATItemPanel{
    
}
@property(nonatomic, strong)  NSAttributedString *infoString;
@property(nonatomic, assign)  float descH;
@end
