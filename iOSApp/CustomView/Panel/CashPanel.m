//
//  CashPanel.m
//  TaxiApp
//
//  Created by Jurayev Nodir on 8/5/15.
//  Copyright (c) 2015 Jurayev Nodir. All rights reserved.
//

#import "CashPanel.h"
#import "ItemListCell.h"

@implementation CashPanel

- (id)init {
    self = [super init];
    if (self) {
        _indexArray = [[NSMutableArray alloc] initWithCapacity:0];
        _sectionsContainer = [[NSMutableDictionary alloc] initWithCapacity:0];
        self.isAlphabetic = NO;
        self.cellType = [[NSString alloc] init];
        
    }
    return self;
}


- (CGFloat)viewWidth {
    CGFloat w = [[UIScreen mainScreen] bounds].size.width;
    CGFloat width = 0;
    width = 300.f*w/320;
    return width;
}

#pragma mark -
#pragma mark View Handling

- (void)dismissPopupViewControllerWithanimationType:(MJPopupViewAnimation)animationType
{
    UIView *popupView = [_sourceView viewWithTag:kMJPopupViewTag];
    UIView *overlayView = [_sourceView viewWithTag:kMJOverlayViewTag];
    
    [self fadeViewOut:popupView sourceView:_sourceView overlayView:overlayView];
}

- (void)presentInController:(UIViewController *)ctrl animationType:(MJPopupViewAnimation)animationType
{
    // customize popupView
    _tableView = [[UITableView alloc] init];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.layer.borderColor = COLOR(203.f, 199.f, 182.f).CGColor;
    _tableView.layer.borderWidth = 1.f;
    _tableView.layer.cornerRadius = 5.f;
    _tableView.alpha = 0.0f;
    if ([_tableView respondsToSelector:@selector(separatorInset)]) {
        _tableView.separatorInset = UIEdgeInsetsZero;
    }
   	CGFloat width = [self viewWidth];
    self.descH = [StyleManager size:[self.infoString string] with:[self viewWidth]-10 fontSize:14].height;

    CGFloat height = self.descH > 350 ? 350 : self.descH + 45;
    
    _tableView.frame = CGRectMake(0.f, 0.f, width, height);
    
    UIView *shadowView = [[UIView alloc] init];
    shadowView.frame = _tableView.frame;
    shadowView.layer.cornerRadius = 5.f;
    shadowView.layer.shadowPath = [UIBezierPath bezierPathWithRect:CGRectOffset(shadowView.frame, 0.f, 7.f)].CGPath;
    shadowView.layer.shadowRadius = 5.f;
    shadowView.layer.shadowColor = [[UIColor blackColor] CGColor];
    shadowView.layer.shadowOpacity =  0.8f;
    shadowView.tag = kMJShadowViewTag;
    
    if (ctrl.parentViewController==nil) {
        _sourceView = ctrl.view;
    }else{
        _sourceView = ctrl.parentViewController.view;
    }
    
    _sourceView.tag = kMJSourceViewTag;
    _tableView.tag = kMJPopupViewTag;
    _sourceView.backgroundColor = [UIColor clearColor];
    
    // Add semi overlay
    UIView *overlayView = [[UIView alloc] initWithFrame:_sourceView.bounds];
    overlayView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    overlayView.tag = kMJOverlayViewTag;
    overlayView.backgroundColor = [UIColor clearColor];
    overlayView.clipsToBounds = YES;
    _tableView.clipsToBounds= YES;
    
    // BackgroundView
    MJPopupBackgroundView *backgroundView = [[MJPopupBackgroundView alloc] initWithFrame:_sourceView.bounds];
    backgroundView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    backgroundView.tag = kMJBackgroundViewTag;
    backgroundView.backgroundColor = [UIColor clearColor];
    backgroundView.alpha = 0.0f;
    
    if (iOS7) {
        overlayView.height+=20;
        backgroundView.height+=20;
    }

    UIButton * dismissButton = [UIButton buttonWithType:UIButtonTypeCustom];
    dismissButton.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    dismissButton.backgroundColor = [UIColor clearColor];
    dismissButton.frame = _sourceView.bounds;
    
    UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:_sourceView.bounds];
    scrollView.backgroundColor = [UIColor clearColor];
    scrollView.userInteractionEnabled = YES;
    CGRect rect = scrollView.frame;
    rect.size.height += 1;
    scrollView.contentSize = rect.size;
    [scrollView addSubview:dismissButton];
    [scrollView addSubview:shadowView];
    [scrollView addSubview:_tableView];
    
    self.alpha = 0.f;
    self.frame = overlayView.frame;
    
    [overlayView addSubview:backgroundView];
    [overlayView addSubview:scrollView];
    [self addSubview:overlayView];
    [_sourceView addSubview:self];
    
    [dismissButton addTarget:self action:@selector(dismissPopupViewControllerWithanimation:) forControlEvents:UIControlEventTouchUpInside];
    
    dismissButton.tag = MJPopupViewAnimationFade;
    [self fadeViewIn:_tableView sourceView:_sourceView overlayView:overlayView];
}

#pragma mark --- Fade
- (void)dismissPopupViewControllerWithanimation:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(panelDismissed)])
        [self.delegate panelDismissed];
    
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
}


- (void)fadeViewIn:(UIView*)popupView sourceView:(UIView*)sourceView1 overlayView:(UIView*)overlayView
{
    UIView *backgroundView = [overlayView viewWithTag:kMJBackgroundViewTag];
    // Generating Start and Stop Positions
    CGSize sourceSize = _sourceView.bounds.size;
    CGSize popupSize = popupView.bounds.size;
    CGRect popupEndRect = CGRectMake((sourceSize.width - popupSize.width) / 2,
                                     (sourceSize.height - popupSize.height) / 2,
                                     popupSize.width,
                                     popupSize.height);
    
    // Set starting properties
    popupView.frame = popupEndRect;
    popupView.alpha = 0.0f;
    
    UIView *shadowView = [overlayView viewWithTag:kMJShadowViewTag];
    shadowView.frame = popupEndRect;
    
    [UIView animateWithDuration:kPopupModalAnimationDuration animations:^{
        backgroundView.alpha = 0.7f;
        popupView.alpha = 1.0f;
        shadowView.alpha = 1.f;
        self.alpha = 1.f;
    } completion:^(BOOL finished) {
    }];
    
}

- (void)fadeViewOut:(UIView*)popupView sourceView:(UIView*)sourceView overlayView:(UIView*)overlayView
{
    UIView *shadowView = [overlayView viewWithTag:kMJShadowViewTag];
    UIView *backgroundView = [overlayView viewWithTag:kMJBackgroundViewTag];
    [UIView animateWithDuration:kPopupModalAnimationDuration animations:^{
        backgroundView.alpha = 0.0f;
        popupView.alpha = 0.0f;
        self.alpha = 0.0f;
        shadowView.alpha = 0.f;
    } completion:^(BOOL finished) {
        [popupView removeFromSuperview];
        [overlayView removeFromSuperview];
        [self removeFromSuperview];
    }];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (self.requeredSelect) {
        
    }
    return nil;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 1) {
        return 45;
    }
    float h =  self.descH;
    return h+10;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 1) {
        NSString *CellIdentifier = [NSString stringWithFormat:@"BtnCell"];
        
        BaseCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (!cell) {
            cell = [[BaseCell alloc] initWithReuseIdentifier:CellIdentifier info:nil];
            float width = _tableView.width/3;
            float x = 0;
            NSArray *items = @[translation(@"tarifCancel"),translation(@"tarifCard"),translation(@"tarifCash")];
            for (int k = 1; k<4; k++) {
                UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(x, 0, width, 40)];
                btn.tag = k;
                [StyleManager styleBtn:btn title:items[k-1]];
                [StyleManager styleBtn:btn titleColor:appColor];
                btn.titleLabel.font = [UIFont helveticeFontSize:14 bold:NO];
                x+= width;
                [cell.contentView addSubview:btn];
                [btn addTarget:self action:@selector(btnClicked:) forControlEvents:UIControlEventTouchUpInside];
            }
        }
        return cell;
        
    }
    NSString *CellIdentifier = [NSString stringWithFormat:@"Cell"];
    
    ItemListCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell) {
        cell = [[ItemListCell alloc] initWithReuseIdentifier:CellIdentifier info:@{kCellWidth:@(_tableView.width+45),kCellHeight:@(self.descH)}];
        cell.descLabel.frame = CGRectMake(5, 5, _tableView.width - 10, self.descH +5);
        cell.descLabel.numberOfLines = 50;
        cell.descLabel.textColor = [UIColor blackColor];
    }
    cell.descLabel.attributedText = self.infoString;
    return cell;
}

#pragma mark - Table view delegate

-(void)btnClicked:(UIButton*)btn{
    NSIndexPath *path = [NSIndexPath indexPathForRow:btn.tag inSection:0];
    if ([self.delegate respondsToSelector:@selector(panel:didSelectAtIndexPath: withObject:)]) {
        [self.delegate panel:self didSelectAtIndexPath:path withObject:nil];
    }
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
}

-(void)dealloc{
    NSLog(@"Dealloc:%@",self.class);
}


@end
