//
//  ErrorObj.h
//  iOSApp
//
//  Created by Jurayev Nodir on 12/18/15.
//  Copyright © 2015 Jurayev Nodir. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ErrorObj : NSObject
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *message;
@property (nonatomic, assign) NSInteger code;

@end
