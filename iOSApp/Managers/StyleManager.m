//
//  StyleManager.m
//  iOSApp
//
//  Created by Admin on 19/01/15.
//  Copyright (c) 2015 Jurayev Nodir. All rights reserved.
//

#import "StyleManager.h"

@implementation StyleManager



+(void)styleBtn:(UIButton*)btn bgImage:(UIImage *)image{
    [btn setBackgroundImage:image forState:UIControlStateNormal];
    [btn setBackgroundImage:image forState:UIControlStateSelected];
    [btn setBackgroundImage:image forState:UIControlStateHighlighted];
    
}

+(void)styleBtn:(UIButton*)btn image:(UIImage *)image{
    [btn setImage:image forState:UIControlStateNormal];
    [btn setImage:image forState:UIControlStateSelected];
    [btn setImage:image forState:UIControlStateHighlighted];
}

+(void)styleBtn:(UIButton *)btn title:(NSString *)title{
    [btn setTitle:title forState:UIControlStateNormal];
    [btn setTitle:title forState:UIControlStateSelected];
    [btn setTitle:title forState:UIControlStateHighlighted];
}
+(void)styleBtn:(UIButton*)btn titleColor:(UIColor*)color {
    [btn setTitleColor:color forState:UIControlStateNormal];
    [btn setTitleColor:color forState:UIControlStateSelected];
    [btn setTitleColor:color forState:UIControlStateHighlighted];
}


+ (UIButton*)defaultBtn:(CGRect)frame title:(NSString *)title{
    UIButton *btn = [[UIButton alloc] initWithFrame:frame];
    [self styleBtn:btn title:title];
    [self styleBtn:btn titleColor:[UIColor whiteColor]];
    btn.clipsToBounds = YES;
    btn.backgroundColor = COLOR(0, 124, 167);
    btn.titleLabel.font = [UIFont helveticeFontSize:20 bold:NO];
    btn.titleLabel.adjustsFontSizeToFitWidth = YES;
    return btn;
}

+(UIImage*)styleImageWithColor:(UIColor*)color size:(CGSize)size{
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

+(NSArray*)styleBackBtn:(NSString*)title selector:(SEL)action target:(id)target{
    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(-5, 0, 60, 25)];
    [btn addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    
    [StyleManager styleBtn:btn title:title];
    btn.titleLabel.font = [UIFont helveticeFontSize:16 bold:NO];
    btn.titleLabel.textColor = [UIColor whiteColor];
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithCustomView:btn];
    UIBarButtonItem* negativeSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    negativeSpacer.width = -10;
    return @[negativeSpacer,backItem];
    
}

+(UIBarButtonItem*)styleMenuItems:(id)controller selector:(SEL)selector{
    UIButton *menuContainer = [[UIButton alloc] initWithFrame:CGRectMake(0, 2, 56, 40)];
    menuContainer.backgroundColor = [UIColor clearColor];
    
    UIButton *button  = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 26, 15)];
    [StyleManager styleBtn:button image:[UIImage imageNamed:@"menu_icon"]];
    [menuContainer addSubview:button];
    
    UIImageView *logo = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo"]];
    logo.frame = CGRectMake(0, 0, 25, 24);
    logo.left = button.left+button.width+5;
    
    logo.centerY = button.centerY = menuContainer.height/2;
    [menuContainer addSubview:logo];
    
    [button addTarget:controller action:selector forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *leftBarItem = [[UIBarButtonItem alloc] initWithCustomView:menuContainer];
    return leftBarItem;

}

+(NSArray*)styleBackItems:(id)controller selector:(SEL)selector{
    UIButton *menuContainer = [[UIButton alloc] initWithFrame:CGRectMake(0, 2, 45, 40)];
    menuContainer.backgroundColor = [UIColor clearColor];
    
    UIButton *button  = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 26, 25)];
    UIImage *image = [[UIImage imageNamed:@"right_arrow"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [StyleManager styleBtn:button image:image];
    button.transform = CGAffineTransformMakeRotation(M_PI);
    [menuContainer addSubview:button];
    
//    UIImageView *logo = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo"]];
//    logo.frame = CGRectMake(0, 0, 25, 24);

    UIButton *logo = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 25, 24)];
    [StyleManager styleBtn:logo image:[UIImage imageNamed:@"logo"]];
    logo.left = button.left+button.width+5;
    logo.centerY = button.centerY = menuContainer.height/2;
    [menuContainer addSubview:logo];
    
    [button addTarget:controller action:selector forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *leftBarItem = [[UIBarButtonItem alloc] initWithCustomView:menuContainer];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    negativeSpacer.width = -10;
    return @[negativeSpacer, leftBarItem];
    
}


+(NSArray*)styleLeftItems:(id)controller selector:(SEL)selector{
    
    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 12, 25)];
    [StyleManager styleBtn:btn bgImage:[UIImage imageNamed:@"back_Icon.png"]];
    [btn addTarget:controller action:selector forControlEvents:UIControlEventTouchUpInside];
    UIView *container = [[UIView alloc] initWithFrame:CGRectMake(5, 0, 44, 44)];
    container.backgroundColor = [UIColor clearColor];
    container.userInteractionEnabled = YES;
    UITapGestureRecognizer *reg = [[UITapGestureRecognizer alloc] initWithTarget:controller action:selector];
    [container addGestureRecognizer:reg];
    
    [container addSubview:btn];
    btn.centerY = container.height/2;
    btn.centerX = container.width/2;
    
    
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:container];
    
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    negativeSpacer.width = -10;
    return @[negativeSpacer, item];
}


+ (CGSize)size:(NSString*)string with:(float)width fontSize:(float)fontsize{
    CGRect rect = [string ? string : @"0" boundingRectWithSize:CGSizeMake(width, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont helveticeFontSize:fontsize bold:NO]} context:nil];
    if (rect.size.height<20) {
        rect.size.height = 20;
    }
    return rect.size;
}

+(UIAlertView *)alertViewWithTitle:(NSString *)text action1:(NSString *)string1 action2:(NSString *)string2{
    return nil;
}

@end
