//
//  SettingsManager.m
//  iOSApp
//
//  Created by Admin on 23/04/15.
//  Copyright (c) 2015 Jurayev Nodir. All rights reserved.
//

#import "SettingsManager.h"


@interface SettingsManager(){
    NSMutableArray *_countryList;
    NSMutableDictionary *_languageDict;
}

@end

@implementation SettingsManager

- (instancetype)init
{
    self = [super init];
    if (self) {
//        NSData *data = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"countries" ofType:@"json"]];
//        NSError *localError = nil;
//        NSArray *parsedObject = [NSJSONSerialization JSONObjectWithData:data options:0 error:&localError];
//        
//        if (localError != nil) {
//            NSLog(@"%@", [localError userInfo]);
//        }
//        _countryList = [[NSMutableArray alloc] init];
//        for (NSDictionary *item in parsedObject) {
//            Country *country = [[Country alloc] initWithDict:item];
//            if (!country.hidden) {
//                [_countryList addObject:country];
//            }
//        }
//        _languageDict = [[NSMutableDictionary alloc] init];
//        
//        [self loadLanguageDict];
//        
//        if ([self userLogin]) {
//            [self userGenerate];
//        }
//        if (![self objForKey:kAppLanguage]) {
//            [self changeLanguage:1]; //russky
//        }
//        [self counterGenerate];
    }

    return self;
}



+ (SettingsManager *)instance {
    static SettingsManager *_instance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _instance = [[self alloc] init];
    });
    
    return _instance;
}

-(void)saveObject:(id)obj key:(NSString *)key{
    [[NSUserDefaults standardUserDefaults] setObject:obj forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(id)objForKey:(NSString *)key{
    return [[NSUserDefaults standardUserDefaults] objectForKey:key];
}

-(BOOL)userLogin{
    id login  = [self objForKey:kAppLoginInfo];
    return login ? YES : NO;
    
}

-(void)logOut{
    NSLog(@"Log out");
}

-(void)registerUser:(NSDictionary*)item{
    
}

#pragma mark - self methods

-(void)counterGenerate{

}

-(void)userGenerate{

}

-(NSArray*)countryList{
    return _countryList;
}

-(void)registerUser {
    [self userGenerate];
    [[NSNotificationCenter defaultCenter] postNotificationName:kUserRegisterNotif object:nil];
    
}

#pragma mark - Language methods

-(NSInteger)appLanguage{
  return [[self objForKey:kAppLanguage] integerValue];
}

-(NSString *)translate:(NSString*)str{
    if (_languageDict[str]) {
        return _languageDict[str];
    }
    return str;
}

-(void)changeLanguage:(NSInteger)index{
    NSInteger oldIndex = [self appLanguage];
    if (oldIndex == index) {
        return;
    }
    [self saveObject:@(index) key:kAppLanguage];
    [self loadLanguageDict];
}

-(void)loadLanguageDict{
    NSString *fileName = appLanguages[[self appLanguage]];
    NSData *data = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:fileName ofType:nil]];
    NSError *localError = nil;
    NSDictionary *langDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:&localError];
    if (localError != nil) {
        NSLog(@"%@", [localError userInfo]);
    }
    if (langDict) {
        [_languageDict setDictionary:langDict];
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kLanguageNotif object:nil];
}
@end
