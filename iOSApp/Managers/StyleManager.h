//
//  StyleManager.h
//  iOSApp
//
//  Created by Admin on 19/01/15.
//  Copyright (c) 2015 Jurayev Nodir. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StyleManager : NSObject

#pragma mark - LeftMenu style

+ (void) styleBtn:(UIButton*)btn image:(UIImage *)image;
+ (void) styleBtn:(UIButton*)btn title:(NSString*)title;
+ (void) styleBtn:(UIButton*)btn bgImage:(UIImage *)image;
+ (void) styleBtn:(UIButton*)btn titleColor:(UIColor*)color;

+ (UIImage*) styleImageWithColor:(UIColor*)color size:(CGSize)size;

+ (CGSize)size:(NSString*)string with:(float)width fontSize:(float)fontsize;

+(NSArray*)styleBackBtn:(NSString*)title selector:(SEL)action target:(id)target;

+(NSArray*)styleBackItems:(id)controller selector:(SEL)selector;
+(UIBarButtonItem*)styleMenuItems:(id)controller selector:(SEL)selector;

+ (UIButton*)defaultBtn:(CGRect)frame title:(NSString *)title;


    
@end
