//
//  SettingsManager.h
//  iOSApp
//
//  Created by Admin on 23/04/15.
//  Copyright (c) 2015 Jurayev Nodir. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSString+MD5Addition.h"

#define appLanguages            @[@"en",@"ru",@"de"]
#define kUserToken              @"autToken"
#define kDeviceToken            [@"deviceToken" MD5]

#define kUserCountry            [@"userCountry" MD5]
#define kAppLanguage            [@"appLanguage" MD5]
#define kAppGeoCoding           [@"appGeocoding" MD5]
#define kAppLoginInfo           [@"loginInfo" MD5]
#define kAppUserPhoneMask       [@"phoneMark" MD5]
#define kAppQiwiPhone           [@"qiwiPhone" MD5]
#define kLanguageNotif          [@"changeLanguageN" MD5]
#define kTester                 [@"tester" MD5]
#define kUserRegisterNotif      [@"kUserRegisterNotif" MD5]
#define kPasswordLength     5
#define kAlphabet @[@"A",@"B",@"C",@"D",@"E",@"F",@"G",@"H",@"I",@"J",@"K",@"L",@"M",@"N",@"O",@"P",@"Q",@"R",@"S",@"T",@"U",@"V",@"W",@"X",@"Y",@"Z"]


//// MASTER TAXI
#define kAppBundleID            @"ru.sedi.customer.master"
#define kAppName                @"Мастер такси"
#define kAppToken               @"7C30D68B-6D40-48D0-A2E3-C84FA8FDB469"
#define kAppSupportPhone        @"+74957400001"
#define kAppServerUrl           @"http://api.msk.sedi.ru//handlers/sedi/api.ashx"
#define kAppleUser              @"AppleTest"
#define kApplePhone             @"+79261400311"
#define kAppleUserKey           @"9a233b97-e5d2-4955-b9ec-fa91dac4b364"
#define kAppGoogleMapsKey       @"AIzaSyAFWiNpRr48fOIGWC4nvdxjFw4eZb1vIhY"
#define kAppSmileLogo           @"small_logo_master.png"

#define homeValueColor COLOR(253, 107, 19)


@interface SettingsManager : NSObject

+ (SettingsManager *)instance;

- (void)saveObject:(id)obj key:(NSString*)key;
- (id)objForKey:(NSString*)key;
- (NSArray*)countryList;

// Language methods
- (NSInteger)appLanguage;
- (void)changeLanguage:(NSInteger)index;
- (NSString *)translate:(NSString*)str;

// User methods
- (BOOL)userLogin;
-(void)userGenerate;
- (void)registerUser;
- (void)logOut;


@end
