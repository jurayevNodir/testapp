//
//  SocketManager.h
//
//
//  Created by Admin on 22/05/15.
//  Copyright (c) 2015 Jurayev Nodir. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSArray+ObjectiveSugar.h"
#import "NSDictionary+ObjectiveSugar.h"
#import "CoreDataManager.h"
#import "NSManagedObject+ActiveRecord.h"

#define kChatServer @"ws://1.1.1.1:8080/customer-gateway/customer"
#define kChatServerPort 8080

@interface SocketManager : NSObject{
    BOOL _sendMessage;

}

+ (SocketManager *)instance;


- (void)reconnect;
- (void)disconnect;
- (void)login:(NSDictionary *)params callback:(Callback)callback;


@end
