//
//  PlSenderHelper.h
//  Planet
//
//  Created by lesia on 8/11/14.
//  Copyright (c) 2014 Planet. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "AFNetworking.h"
#import "Enums.h"


@interface ConnectionManager : AFHTTPRequestOperationManager {}

+ (ConnectionManager *)instance;

- (void) orderStatuses:(Callback)callback;
- (void) getTarifs:(Callback)callback;


- (void) authWithPhone:(NSDictionary*)dict callback:(Callback)callback;
- (void) authWithSMS:(NSDictionary*)dict callback:(Callback)callback;
- (void) createOrder:(NSDictionary*)dict callback:(Callback)callback;

// order methods
- (void)activeOrders:(Callback)callback;
- (void)orderHistory:(Callback)callback;


@end
