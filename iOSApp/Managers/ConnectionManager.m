//
//  PlSenderHelper.m
//  Planet
//
//  Created by lesia on 8/11/14.
//  Copyright (c) 2014 Planet. All rights reserved.
//

#import "ConnectionManager.h"



@interface ConnectionManager () {

}

@end

@implementation ConnectionManager {}

//#define kAppServerUrl  @"http://api.sedi.ru/handlers/sedi/api.ashx"
//#define kAppServerUrl  @"http://api.msk.sedi.ru/handlers/sedi/api.ashx"


+ (ConnectionManager *)instance {
    static ConnectionManager *_instance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _instance = [[self alloc] initWithBaseURL:[NSURL URLWithString:kAppServerUrl]];
    });
    
    return _instance;
}

-(id)initWithBaseURL:(NSURL *)url{
    self = [super initWithBaseURL:url];
    if (self) {
        self.responseSerializer = [AFJSONResponseSerializer serializer];
        self.responseSerializer.stringEncoding = NSUTF8StringEncoding;
        
        self.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html",@"text/plain", nil];
        
        self.requestSerializer = [AFJSONRequestSerializer serializer];
        self.requestSerializer.stringEncoding = NSUTF8StringEncoding;


    }
    return self;
}




-(void)requestPath:(NSString *)path  params:(NSDictionary *)par type:(RequestType)type callback:(Callback)callback{
    self.requestSerializer.HTTPShouldHandleCookies = NO;
    NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithDictionary:par];
    params[@"key"] = kAppToken;
    params[@"dateformat"] = @"iso";
    NSMutableDictionary *logDict = [[NSMutableDictionary alloc] init];
    logDict[@"params"] = params;
    if (path) {
        
        logDict[@"path"] = path;
    }
    if ([[SettingsManager instance] objForKey:kUserToken]) {
        params[@"userKey"] = [[SettingsManager instance] objForKey:kUserToken];
    }
    if ([params[@"q"] isEqualToString:@"activation_key"]) {
        self.requestSerializer.HTTPShouldHandleCookies = YES;
    }
        

    
    if (type == RequestTypePOST) {
        
        [self POST:path parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
            DLog(@"Request:%@",logDict);
            DLog(@"Response:%@",responseObject);
            
            [self parsing:responseObject error:nil callback:callback];
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            DLog(@"Request:%@",logDict);
            DLog(@"Error:%@",error.userInfo);
            [self parsing:nil error:error callback:callback];
        }];
        
        
    } else if(type== RequestTypeGET){
        [self GET:path parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
            DLog(@"Request:%@",logDict);
            DLog(@"Response:%@",responseObject);
            [self parsing:responseObject error:nil callback:callback];
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            DLog(@"Error:%@",error.userInfo);
            [self parsing:nil error:error callback:callback];
        }];
        
    } else if (type == RequestTypePUT){
        
        [self PUT:path parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
            DLog(@"Request:%@",logDict);
            DLog(@"Response:%@",responseObject);
            [self parsing:responseObject error:nil callback:callback];
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            DLog(@"Request:%@",logDict);
            DLog(@"Error:%@",error.userInfo);
            [self parsing:nil error:error callback:callback];
        }];
        
    } else if(type == RequestTypeDELETE){
        
        [self DELETE:path parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            DLog(@"Request:%@",logDict);
            DLog(@"Response:%@",responseObject);
            [self parsing:responseObject error:nil callback:callback];
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            DLog(@"Request:%@",logDict);
            DLog(@"Error:%@",error.userInfo);
            [self parsing:nil error:error callback:callback];
        }];
        
    }
    
}


-(void)parsing:(id)response error:(NSError *)error callback:(Callback)callback{
    ErrorObj *resultError= nil;
    id resultOb = nil;
    if (response) {
        NSInteger success = [response[@"Success"] integerValue];
        if (success == 1) {
            resultOb = response;
        } else{
            NSString *errorString = NULL_TO_NIL(response[@"Message"]);
            if (errorString.length) {
                resultError = [[ErrorObj alloc] init];
                resultError.title = response[@"Error"][@"Name"];
                resultError.message = errorString;
                resultOb = nil;
            }
            
        }
    }
    if (error) {
        resultError = [[ErrorObj alloc] init];
        resultError.title = @"Error";
        resultError.message = error.localizedFailureReason;
        resultError.code = error.code;
    }
    if (callback) {
        callback(resultOb,resultError);
    }
    
}

#pragma mark - Defauly methods

-(void)orderStatuses:(Callback)callback{
    [self requestPath:@"" params:@{@"q": @"get_enums"} type:RequestTypeGET callback:^(id response, ErrorObj *error) {
        if (callback) {
            callback(response,error);
        }
    }];
}

-(void)getTarifs:(Callback)callback{
    [self requestPath:@"" params:@{@"q": @"get_tariffs"} type:RequestTypeGET callback:^(id response, ErrorObj *error) {
        if (callback) {
            callback(response,error);
        }
    }];
}

#pragma mark - Order methods

-(void)createOrder:(NSDictionary *)dict callback:(Callback)callback {
    [self requestPath:@"" params:dict type:RequestTypeGET callback:^(id response, ErrorObj *error) {
        if (callback) {
            callback(response,error);
        }
    }];
}

-(void)orderHistory:(Callback)callback{
    [self requestPath:@"" params:@{@"q":@"get_orders",@"from":@"01.01.2010",@"to":@"01.01.2016"} type:RequestTypeGET callback:^(id response, ErrorObj *error) {
        if (callback) {
            callback(response,error);
        }
    }];
}

-(void)activeOrders:(Callback)callback{
    [self requestPath:@"" params:@{@"q":@"get_orders"} type:RequestTypeGET callback:^(id response, ErrorObj *error) {
        if (callback) {
            callback(response,error);
        }
    }];
}

#pragma mark - AuthMethods

-(void)authWithPhone:(NSDictionary *)dict callback:(Callback)callback{
    [self requestPath:@"" params:dict type:RequestTypeGET callback:^(id response, ErrorObj *error) {
        if (callback) {
            callback(response,error);
        }
    }];
}

-(void)authWithSMS:(NSDictionary *)dict callback:(Callback)callback{
    self.requestSerializer.HTTPShouldHandleCookies = YES;
    [self requestPath:@"" params:dict type:RequestTypeGET callback:^(id response, ErrorObj *error) {
        NSArray *cookies = [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookiesForURL: [NSURL URLWithString:kAppServerUrl]];
        for (NSHTTPCookie *cookie in cookies)
        {
            if ([cookie.name isEqualToString:@"auth"]) {
                [[SettingsManager instance] saveObject:cookie.value ? cookie.value : @"" key:kUserToken];
            }
            [[NSHTTPCookieStorage sharedHTTPCookieStorage] deleteCookie:cookie];
        }
        if (callback) {
            callback(response,error);
        }
    }];
    
}


@end
