//
//  SocketManager.m
//
//
//  Created by Admin on 22/05/15.
//  Copyright (c) 2015 Jurayev Nodir. All rights reserved.
//

#import "SocketManager.h"
#import "SocketIO.h"
#import "SRWebSocket.h"

#import <AudioToolbox/AudioServices.h>
#import <AVFoundation/AVFoundation.h>

@interface SocketManager()<SocketIODelegate,SRWebSocketDelegate>{
    
    SocketIO *_socketIO;
    
    SRWebSocket *_webSocket;
    
    NSMutableArray *_messages;
    BOOL _complete;
}

@property (nonatomic, copy) void(^Callback)(id response,ErrorObj *error);
@property (nonatomic, strong) AVAudioPlayer *player;

@end

@implementation SocketManager

+ (SocketManager *)instance {
    static SocketManager *_instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _instance = [[self alloc] init];
    });
    
    return _instance;
}



- (instancetype)init{
    self = [super init];
    if (self) {
        NSURL *url = [NSURL URLWithString:kChatServer];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        _webSocket = [[SRWebSocket alloc] initWithURLRequest:request];
        _webSocket.delegate = self;
        [_webSocket open];
//        _messages = [[NSMutableArray alloc] init];
//        // create socket.io client instance
//        _socketIO = [[SocketIO alloc] initWithDelegate:self];
//        
//        // you can update the resource name of the handshake URL
//        // see https://github.com/pkyeck/socket.IO-objc/pull/80
//        // if you want to use https instead of http
//        _socketIO.useSecure = NO;
//        [_socketIO connectToHost:kChatServer onPort:kChatServerPort withParams:[self params]];
        
    }
    
    return self;
}

- (void)reconnect {
    [_webSocket close];
    [_webSocket open];
//    if (!_socketIO.isConnected) {
//        [_socketIO connectToHost:kChatServer
//                                onPort:kChatServerPort
//                            withParams:[self params]];
//    }
}

-(void)disconnect{
    [_webSocket close];
//    if (_socketIO.isConnected) {
//        [_socketIO disconnect];
//    }
}


-(void)login:(NSDictionary *)params callback:(Callback)callback{

   NSData *data = [NSKeyedArchiver archivedDataWithRootObject:params];
    [_webSocket send:data];
}

#pragma mark - Socket Rocket metods

- (void)webSocket:(SRWebSocket *)webSocket didReceiveMessage:(id)message{
    NSDictionary *dict = [message JSONValue];
    NSLog(@"%@",dict);
}

- (void)webSocketDidOpen:(SRWebSocket *)webSocket{
    NSLog(@"Open");
    
}
- (void)webSocket:(SRWebSocket *)webSocket didFailWithError:(NSError *)error{
    NSLog(@"Error:%@",error);
}

- (void)webSocket:(SRWebSocket *)webSocket didCloseWithCode:(NSInteger)code reason:(NSString *)reason wasClean:(BOOL)wasClean{
    NSLog(@"Close:%@",reason);
}

- (void)webSocket:(SRWebSocket *)webSocket didReceivePong:(NSData *)pongPayload{
    NSLog(@"%@",pongPayload);
}



#pragma mark - User Methods

-(void)sendMessage:(NSDictionary *)params callback:(Callback)callback{
    
    NSLog(@"SEND message:\n%@",params);
    _sendMessage = YES;
    [_socketIO sendEvent:@"send_message" withData:params andAcknowledge:^(id argsData) {
        NSLog(@"%@",argsData);
    }];
}

-(void)search:(NSDictionary *)params callback:(Callback)callback{
    self.Callback = [callback copy];
    [_socketIO sendEvent:@"search" withData:params andAcknowledge:^(id argsData) {
        NSLog(@"%@",argsData);
    }];

}

-(void)sendNotifcation{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"newMessage" object:nil];
}


# pragma mark -
# pragma mark socket.IO-objc delegate methods

- (void) socketIODidConnect:(SocketIO *)socket{
    NSLog(@"socket.io connected.");
}

- (void) socketIODidDisconnect:(SocketIO *)socket disconnectedWithError:(NSError *)error{
    if ([error code] == SocketIOUnauthorized) {
        NSLog(@"not authorized");
    } else {
        [self performSelector:@selector(reconnect) withObject:nil afterDelay:5];
    }
    [self sendNotifcation];

}
- (void) socketIO:(SocketIO *)socket didReceiveMessage:(SocketIOPacket *)packet{
    NSLog(@"Message from Server:%@",packet.args);
    [self sendNotifcation];

}

- (void) socketIO:(SocketIO *)socket didReceiveJSON:(SocketIOPacket *)packet{
    if ([packet.args isKindOfClass:[NSArray class]]) {
    }
}

- (void) socketIO:(SocketIO *)socket didSendMessage:(SocketIOPacket *)packet{

}

- (void) socketIO:(SocketIO *)socket onError:(NSError *)error{
    NSLog(@"%@",error);
    [self reconnect];
    [self sendNotifcation];
}

- (void) socketIO:(SocketIO *)socket didReceiveEvent:(SocketIOPacket *)packet {
    NSLog(@"didReceiveEvent()->>>>>%@",packet.dataAsJSON);
    NSArray *items = packet.dataAsJSON;
    [self receiveEvent:items];
}

#pragma mark - Parser methods

-(void)receiveEvent:(NSArray *)items{
    NSLog(@"%@",items);
}

-(void)soundSendMessage{
//    [self playSoundFile:@"chatMessage.mp3"];
//    AudioServicesPlaySystemSound (1202);
}

-(void)soundReceiveChatMessage{
    [self playSoundFile:@"chatMessage.mp3"];
    [self vibrate];
}

-(void)soundReceiveMessage{
    [self playSoundFile:@"message.mp3"];
}

-(void)soundNewRequest{
    [self playSoundFile:@"request.mp3"];
    [self vibrate];
}

-(void)vibrate{
    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
}

-(void)playSoundFile:(NSString *)fileName{
    NSString *soundFilePath = [NSString stringWithFormat:@"%@/%@",
                               [[NSBundle mainBundle] resourcePath],fileName];
    NSURL *soundFileURL = [NSURL fileURLWithPath:soundFilePath];
    self.player = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL
                                                                   error:nil];
    self.player.numberOfLoops = 0;
    [self.player play];
}





@end
